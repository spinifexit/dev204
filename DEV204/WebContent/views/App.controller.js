jQuery.sap.require("model.DataService");

sap.ui.controller("views.App", {

	onInit : function() {
		this.createGlobalNavigationModel();
		this.createLoadingIndicators();

		var thisInstance = this;
		// Initialize history management
		sap.ui.getCore().getEventBus().subscribe("navApp", "to", this.navAppTo, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("navApp", "back", this.navAppBack, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("navWorkList", "to", this.navWorkListTo, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("navWorkList", "back", this.navWorkListBack, thisInstance);
		// Initialize the listener to show loading indicators
		sap.ui.getCore().getEventBus().subscribe("loadingEntityList", "load", this.showEntityListIndicator, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("loadingEntityList", "loaded", this.hideEntityListIndicator, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("loadingEntityContent", "load", this.showEntityContentIndicator, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("loadingEntityContent", "loaded", this.hideEntityContentIndicator, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("savingEntries", "load", this.showSavingEntriesIndicator, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("savingEntries", "loaded", this.hideSavingEntriesIndicator, thisInstance);
		// Communication Error
		sap.ui.getCore().getEventBus().subscribe("err", "communicationFailure", this.showCommunicationFailure, thisInstance);
	},

	navBack : function(channelId, eventId, data) {
		jQuery.sap.history.back();
		jQuery.sap.log.info("navBack");
	},
	/**
	 * [CONTROLLER] Handle the navigation when a new page is requested. This
	 * navigation is global between pages unlike the navWorkListTo that handles
	 * the navigation within the splitApp.
	 * 
	 * @see navWorkListTo
	 * @param channelId
	 *            {String} The type of navigation (usually "app")
	 * @param eventId
	 *            {String} The name of the even for the navigation (used by the
	 *            different view upon subscribing to the event of the event bus
	 *            of the application)
	 * @param data
	 *            {JSon} A JSon object representing the data to be transferred
	 *            along with the navigation
	 */
	navAppTo : function(sChannelId, sEventId, data) {

		var app;
		var viewName;
		var viewId;
		var dataObject;
		var view;

		app = this.getView().app;
		viewName = data.viewName;
		viewId = data.viewId;
		dataObject = data.dataObject;

		// If no specific viewId is provided, the view name will be used as
		// a viewId. This is used for creating more instances
		// of the same view.
		if (!viewId) {
			viewId = "id." + viewName;
		}
		try {
			if (!viewId) {
				throw new Error("Error in App.controller.navLaunchTo. The view name is empty.");
				return;
			}

			if (!sap.ui.getCore().byId(viewId)) {
				// The view has not been created.
				if (!viewName) {
					throw new Error("Error in App.controller.navLaunchTo. The view name is empty.");
					return;
				}
				view = sap.ui.jsview(viewId, viewName);
				app.addPage(view);
			}
			// Navigate now to the respective page
			app.to(viewId, "slide", dataObject, null);
		} catch (e) {
			alert(e.message);
		}
	},
	/**
	 * [CONTROLLER] Handle the navigation when back is pressed. This navigation
	 * back is global between pages unlike the navWorkListBack that handles the
	 * back button pressed within the left menu navigation.
	 * 
	 * @param channelId
	 *            {String} The type of navigation (usually "app")
	 * @param eventId
	 *            {String} The name of the even for the navigation (used by the
	 *            different view upon subscribing to the event of the event bus
	 *            of the application)
	 * @param data
	 *            {JSon} A JSon object representing the data to be transferred
	 *            along with the navigation
	 */
	navAppBack : function(sChannelId, sEventId, data) {
		var app;
		app = this.getView().app;
		app.back(data);
	},
	
	navWorkListTo : function(sChannelId, sEventId, data) {
		var app;
		var viewName;
		var viewId;
		var dataObject;
		var isMaster = true;

		app = this.getView().workList;
		viewName = data.viewName;
		viewId = data.viewId;

		dataObject = {
			standAlone : data.standAlone
		};

		// If no specific viewId is provided, the view name will be used as
		// a viewId. This is used for creating more instances
		// of the same view.
		if (!viewId) {
			viewId = "id." + viewName;
		}
		try {
			if (!viewId) {
				throw new Error("Error in App.controller.navWorkListTo. The view name is empty.");
				return;
			}

			isMaster = (viewId.indexOf("views.worklist.") !== -1);

			if (!sap.ui.getCore().byId(viewId)) {
				// The view has not been created.
				if (!viewName) {
					throw new Error("Error in App.controller.navWorkListTo. The view name is empty.");
					return;
				}
				view = sap.ui.jsview(viewId, viewName);
			}

			(isMaster) ? app.addMasterPage(sap.ui.getCore().byId(viewId)) : app.addDetailPage(sap.ui.getCore().byId(viewId));
			// Navigate now to the respective page
			(isMaster) ? app.toMaster(viewId, dataObject) : app.toDetail(viewId, dataObject);
		} catch (e) {
			alert(e.message);
		}
	},
	/**
	 * [CONTROLLER] Handle the navigation when back is pressed within the
	 * splitApp
	 * 
	 * @param channelId
	 *            {String} The type of navigation (usually "app")
	 * @param eventId
	 *            {String} The name of the even for the navigation (used by the
	 *            different view upon subscribing to the event of the event bus
	 *            of the application)
	 * @param data
	 *            {JSon} A JSon object representing the data to be transferred
	 *            along with the navigation
	 */
	navWorkListBack : function(sChannelId, sEventId, data) {
		var app;
		app = this.getView().workList;
		app.backMaster();
	},
	
	createGlobalNavigationModel:function(){
		sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel([]), "globalNavigationModel");
	},
	
	createLoadingIndicators : function() {
		this.entityListIndicator = new sap.m.BusyDialog({
			text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_LOADENTITYLIST')
		});
		this.entityContentIndicator = new sap.m.BusyDialog({
			text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_LOADENTITYCONT')
		});
		this.savingEntriesIndicator = new sap.m.BusyDialog({
			text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SAVINGENTRIES')
		});
	},

	showEntityListIndicator : function() {
		this.entityListIndicator.open();
	},

	hideEntityListIndicator : function() {
		this.entityListIndicator.close();
	},

	showSavingEntriesIndicator : function() {
		this.savingEntriesIndicator.open();
	},

	hideSavingEntriesIndicator : function() {
		this.savingEntriesIndicator.close();
	},

	showEntityContentIndicator : function() {
		this.entityContentIndicator.open();
	},

	hideEntityContentIndicator : function() {
		this.entityContentIndicator.close();
	},

	showCommunicationFailure : function(sChannelId, sEventId, data) {

	}

});