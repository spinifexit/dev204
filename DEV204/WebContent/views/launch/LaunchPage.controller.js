jQuery.sap.require("controls.roundedActionTile.RoundedActionTile");

sap.ui.controller("views.launch.LaunchPage", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf views.launch.LaunchPage
*/
	onInit: function() {
		this.isBuilt = false;
	},
	
	onBeforeShow : function(oEvent) {
		var goToApp = "BROWSERTOOL";//jQuery.sap.getUriParameters().get("launchApp");
		
		if(goToApp !== ""){
			this.openApp(goToApp);
		}
		
		if(!this.isBuilt){
			this.isBuilt = true;
			this.aTiles = [
			               	{
			               		title: "BROWSERTOOL",
			               		icon: "table-view"
			               	}
			              ];
			for (var c in this.aTiles) {
				// Create the rounded tile
				var tileItem = new controls.RoundedActionTile();
				tileItem.setTitle(sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_'+this.aTiles[c]["title"]));
				tileItem.setIcon("sap-icon://" + this.aTiles[c]["icon"]);
				if (this.aTiles[c]["cssClass"])
					tileItem.setCssClass(this.aTiles[c]["cssClass"]);
				// Keep a reference of the UI5 tile object for later processing
				this.aTiles[c].object = tileItem;
				// Attach the event handler for the click on the tile
				tileItem.attachPress({
					index : c
				}, this.handleNavigation, this);
				// Add the tile to the container
				this.getView().oTilesContainer.addTile(tileItem);
			} 
		}
	},
	
	handleNavigation:function(oEvent, oParams){
		var clickedTile = this.aTiles[parseInt(oParams.index)];
		this.openApp(clickedTile.title);
	},
	
	openApp:function(app){
		switch(app){
		case "BROWSERTOOL":
			var globalNavigationModel = sap.ui.getCore().getModel("globalNavigationModel").getData();
			globalNavigationModel.push({from:"id.views.launch.LaunchPage", to: "id.views.launch.workListPage"});
			sap.ui.getCore().getEventBus().publish("navWorkList", "to", {
				viewId : "id.views.worklist.worklist",
				viewName : "views.worklist.worklist"
			});
			sap.ui.getCore().getEventBus().publish("navWorkList", "to", {
				viewId : "id.views.detail.details",
				viewName : "views.detail.details"
			});
			sap.ui.getCore().getEventBus().publish("navApp", "to", {
				viewId : "id.views.launch.workListPage",
				viewName : "views.launch.workListPage"
			});
			break;
		}
	}

});