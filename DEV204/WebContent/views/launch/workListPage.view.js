/**
 * @lends view.launch.WorkListPage.prototype
 */
sap.ui.jsview("views.launch.workListPage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 */
	getControllerName : function() {
		return "views.launch.workListPage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 */
	createContent : function(oController) {
		var page = new sap.m.Page();
		oController._pageId = page.getId();
		page.setShowHeader(false);
		page.setEnableScrolling(false);
		page.setTitle(sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_MAINTITLE'));
		// ViewData contains the worklist navigation container
		page.addContent(this.getViewData());
		return page;
	}

});