sap.ui.jsview("views.App", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf views.App
	*/ 
	getControllerName : function() {
		return "views.App";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf views.App
	*/ 
	createContent : function(oController) {
		var startView;
		// Create the main app
		this.app = new sap.m.App();
		// Create the first page (which is the launch pad)
		startView = sap.ui.jsview("id." + "views.launch.LaunchPage", "views.launch.LaunchPage");
		this.app.addPage(startView);
		this.app.setInitialPage(startView.getId());
		// Create the second level navigation container which is included in main app
		this.workList = new sap.m.SplitApp();
		// Add the details view frame to the worklist details
		var workListView = sap.ui.view({
			id : "id.views.launch.workListPage",
			viewName : "views.launch.workListPage",
			type : sap.ui.core.mvc.ViewType.JS,
			viewData : this.workList
		});
		this.app.addPage(workListView);
		// Navigate to the first page
		this.app.to(startView.getId());
		return this.app;
	}

});