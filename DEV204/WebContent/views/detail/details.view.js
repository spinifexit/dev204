sap.ui.jsview("views.detail.details", {

	/** Specifies the Controller belonging to this View. 
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf views.details
	 */
	getControllerName : function() {
		return "views.detail.details";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	 * Since the Controller is given to this method, its event handlers can be attached right away. 
	 * @memberOf views.details
	 */
	createContent : function(oController) {
		this.editSwitch = new sap.m.Switch({
			state : false,
			change : oController.switchChanged
		});
		this.page = new sap.m.Page({
			headerContent : [
						new sap.m.Label({
							text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_EDITMODE'),
							textAlign : "Right"
						}), this.editSwitch
			],
			showNavButton:true,
			navButtonPress: oController.onBackClick
		}).addStyleClass("sapUiSizeCompact");

		this.toolbar = new sap.m.Toolbar({});
		this.page.setFooter(this.toolbar);

		return this.page;
	}

});