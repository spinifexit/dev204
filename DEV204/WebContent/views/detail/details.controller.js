sap.ui.controller("views.detail.details", {
	//navigation history
	navHistory: [],
	navHistoryData: {},
	entityMetadata : {},
	// ******************************************************************************************
	// * STANDARD EVENTS *
	// ******************************************************************************************
	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf views.details
	 */
	onInit : function() {
		
		this.editMode = false;
		this.saveLog = [];
		this.editedLines = [];
		
		var thisInstance = this;
		sap.ui.getCore().getEventBus().subscribe("app", "checkEditedValues", thisInstance.checkEditedValues, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "loadEntityContent", thisInstance.loadEntityContent, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("data", "entityDataLoaded", thisInstance.entityDataLoadedSuccess, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityDataLoaded", thisInstance.entityDataLoadedError, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "communicationFailure", thisInstance.entityDataLoadedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "entityDataCreated", thisInstance.entityDataCreated, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityDataCreated", thisInstance.entityDataCreatedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "entityDataUpdated", thisInstance.entityDataUpdated, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityDataUpdated", thisInstance.entityDataUpdatedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "entityDataDeleted", thisInstance.entityDataDeleted, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityDataDeleted", thisInstance.entityDataDeletedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "translationDataCreated", thisInstance.translationDataCreated, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "translationDataCreated", thisInstance.translationDataCreatedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "translationDataUpdated", thisInstance.translationDataUpdated, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "translationDataUpdated", thisInstance.translationDataUpdatedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("app", "sortFilterValueList", thisInstance.sortFilterValueList, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "setEditable", thisInstance.setEditable, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "showValueHelp", thisInstance.showValueHelp, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "valueChanged", thisInstance.valueChanged, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "reloadTable", thisInstance.reloadTable, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("app", "translationTableRebuildBinding", thisInstance.translationTableRebuildBinding, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "translationTableSaveEntries", thisInstance.translationTableSaveEntries, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "translationTableDeleteEntries", thisInstance.translationTableDeleteEntries, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "translationValueChanged", thisInstance.translationTableValueChanged, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "translationTableEntityDataDeleted", thisInstance.translationTableEntityDataDeleted, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "translationTableEntityData", thisInstance.translationTableEntityDataDeletedError, thisInstance);

		sap.ui.getCore().getEventBus().subscribe("data", "entityExpandedDataLoaded", thisInstance.entityExpandedDataLoaded, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityExpandedDataLoaded", thisInstance.entityExpandedDataLoaded, thisInstance);
	
		//backbutton implementation
		sap.ui.getCore().getEventBus().subscribe("app", "goBack", thisInstance.goBack, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "loadEntityContent", thisInstance.manageNavHistory, thisInstance);
		//remove the backbutton on init, there is no need for it yet
		this.getView().page.setShowNavButton(false);
	},
	/**
	 * subscribe to the same event as load entity content. 
	 * activate each time an entity is loaded
	 * @param channelId
	 * @param eventId
	 * @param data
	 */
	manageNavHistory: function(channelId, eventId, data, type){
		this.isShowBackButton();
		if(eventId == 'loadEntityContent'){
			var navObj = {
				channelId: channelId,
				eventId: eventId,
				data: data,
				type: (typeof type == 'undefined'? null: type)
			}; 
			var historyKey = data.title;
			if(data.filter != ''){
				historyKey = historyKey + '_' + data.filter;
			}
			this.navHistory.push(historyKey);
			//set the navObject
			this.navHistoryData[historyKey] = navObj;
		}
	},
	/**
	 * event handler for the back button click from the ui
	 * @param oEvent
	 */
	onBackClick: function(oEvent){
		sap.ui.getCore().getEventBus().publish("app", "goBack");
	},
	/**
	 * event listener for back event 
	 * this moves the history back by 1 step.
	 * @param sChannelId
	 * @param sEventId
	 * @param oData
	 */
	goBack:function(sChannelId, sEventId, oData){
		//get the last entry
		this.isShowBackButton();
		if(this.navHistory.length > 0){
			this.navHistory.splice(this.navHistory.length -1, 1);
			//get the second to the last element since the current is included
			var lastEntry = this.navHistory[this.navHistory.length - 1];
			this.navHistory.splice(this.navHistory.length -1, 1);
			//if type is null then it was undefined to start with therefore it is a parent navigation item
			if(this.navHistoryData[lastEntry].data.filter == ''){
				sap.ui.getCore().getEventBus().publish("app", "listObjectSelected", {
					toSelect : lastEntry
				});
			}
			sap.ui.getCore().getEventBus().publish(this.navHistoryData[lastEntry].channelId, this.navHistoryData[lastEntry].eventId, this.navHistoryData[lastEntry].data);
		}
	},
	/**
	 * Checks if the back button should be shown or not
	 * @returns {Boolean}
	 */
	isShowBackButton: function(){
		var toShow = (this.navHistory.length > 0);
		this.getView().page.setShowNavButton(toShow);
		return toShow;
	},
	// ******************************************************************************************
	// * BACKEND CALLS *
	// ******************************************************************************************
	reloadTable : function(channelId, eventId, data) {
		this.loadEntityContent("app", "loadEntityContent", {
			metadata : this.entityMetadata,
			filter : this.entityFilter,
			title : this.getView().page.getTitle()
		});
	},

	loadEntityContent : function(channelId, eventId, data) {
		this.entityMetadata = {};
		this.setPageTitle(data.title);
		// Show the loading dialog
		sap.ui.getCore().getEventBus().publish("loadingEntityContent", "load");
		// Retrieve entity metadata
		this.entityMetadata = data.metadata;
		// Retrieve the filter (optional)
		this.entityFilter = data.filter;
		// Build the object for the callback
		var content = {};
		content.serviceType = 'entityDataLoaded';
		// Build the oData query
		var url = this.entityMetadata.name + 's';
		var parameters = [];
		// Check if expands are needed
		var expandToDo = null;
		if ((expandToDo = this.getEntityExpands()) !== "")
			parameters.push("$expand=" + expandToDo);
		parameters.push(this.entityFilter);
		// Call the OData API
		model.DataService.loadData(content, url, null, parameters);
	},

	getExpandedValue : function(propertyToExpand, entityId, toObject, type, object, property) {
		var content = {};
		content.serviceType = 'entityExpandedDataLoaded';
		content.propertyExpanded = propertyToExpand;
		content.toObject = toObject;
		content.type = type;
		content.object = object;
		content.property = property;
		// Build the oData query
		var url = this.entityMetadata.name + 's(' + entityId + ')';
		var parameters = [];
		// Check if expands are needed
		parameters.push("$expand=" + propertyToExpand);
		// Call the OData API
		model.DataService.loadData(content, url, null, parameters);
	},

	// ******************************************************************************************
	// * BACKEND CALLS RESPONDERS *
	// ******************************************************************************************
	entityDataLoadedSuccess : function(channelId, eventId, data) {
		this.buildContentDisplay(this.entityMetadata.property, this.entityMetadata.key.propertyRef, this.entityMetadata.navigationProperty, data.results);
		sap.ui.getCore().getEventBus().publish("loadingEntityContent", "loaded");
	},

	entityDataLoadedError : function(channelId, eventId, data) {
		sap.ui.getCore().getEventBus().publish("loadingEntityContent", "loaded");
		new sap.m.Dialog({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_COMMERROR'),
			type : "Message",
			content : new sap.m.Text({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_LOADERROR', [
					sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_ENTITYCONTWITHNAME', [
						this.getView().page.getTitle()
					])
				])
			}),
			beginButton : new sap.m.Button({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_OK'),
				press : function(oEvent) {
					oEvent.getSource().getParent().close();
				}
			}),
			state : "Error"
		}).open();
	},

	entityExpandedDataLoaded : function(sChannelId, sEventId, oData) {
		var filterValue = "";
		if (sChannelId === "err") {
			// Handle Error
		} else {
			if (oData.dataObject.params.type === "nav") {
				if (oData.results[oData.dataObject.params.propertyExpanded] !== null) {
					var results = oData.results[oData.dataObject.params.propertyExpanded].results;
					filterValue = "$filter=";
					if (results) {
						for ( var i in results) {
							filterValue += "Id+eq+" + results[i].Id + "+or+";
						}
						filterValue = filterValue.substring(0, filterValue.length - 4);
					} else {
						filterValue += "Id+eq+" + oData.results[oData.dataObject.params.propertyExpanded].Id;
					}
				}
				if (oData.dataObject.params.toObject === "FieldGroup2") {
					oData.dataObject.params.toObject = "FieldGroup";
				}
				// Select the corresponding entity in the worklist
				sap.ui.getCore().getEventBus().publish("app", "listObjectSelected", {
					toSelect : oData.dataObject.params.toObject
				});
				// Load the data
				sap.ui.getCore().getEventBus().publish("app", "loadEntityContent", {
					title : oData.dataObject.params.toObject,
					metadata : this.getEntityFullMetadata(oData.dataObject.params.toObject),
					filter : filterValue
				});
			} else if (oData.dataObject.params.type === "edit") {
				var results = oData.results[oData.dataObject.params.propertyExpanded].results;
				oData.dataObject.params.object[oData.dataObject.params.property].results = results;
				this.buildPopupForEdit(oData.dataObject.params.object, oData.dataObject.params.property, oData.dataObject.params.toObject, results,oData.dataObject.params.type);
			}
		}
	},
	// ******************************************************************************************
	// * PAGE RENDERING *
	// ******************************************************************************************

	checkEditedValues : function(channelId, eventId, data) {
		if (this.hasUnsavedChanges()) {
			data.navigateToEntity = true;
			this.showLossChangePopup(channelId, eventId, data);
		} else {
			sap.ui.getCore().getEventBus().publish("app", "proceedWithNavigation", {
				title : data.title,
				metadata : data.metadata
			});
		}
	},

	switchChanged : function(param) {
		sap.ui.getCore().getEventBus().publish("app", "setEditable", {
			editMode : param.getParameter('state')
		});
	},

	hasUnsavedChanges : function() {
		if (this.editMode && this.editedLines && this.editedLines.length > 0)
			return true;
		return false;
	},

	changeMode : function(channelId, eventId, data) {
		this.editedLines = [];
		this.editMode = data.editMode;
		// Change the table edit mode
		if (this.table) {
			this.editMode ? this.table.setMode(sap.m.ListMode.MultiSelect) : this.table.setMode(sap.m.ListMode.None);
			this.table.unbindItems();
			this.table.removeAllColumns();
			// re-apply column sorting
			this.table.getModel().getData().columns.sortBy("-isKey", "name");
			this.table.getModel().getData().columns = this.setRecordStatusFirst(this.table.getModel().getData().columns);

			this.editMode ? this.bindColumnsForEdit(this.table) : this.bindColumns(this.table);
			this.editMode ? this.bindRowsForEdit(this.table) : this.bindRows(this.table);
		}
		// Display or hide the buttons in the footer
		this.addRemoveFooterButtons(this.editMode);
	},

	showLossChangePopup : function(channelId, eventId, data) {
		var dialog = new sap.m.Dialog({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_UNSAVEDCHANGESTITLE'),
			type : 'Message',
			state : "Warning",
			content : new sap.m.Text({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_UNSAVEDCHANGES')
			}),
			beginButton : new sap.m.Button({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_CONFIRM'),
				press : function(oEvent) {
					this.editedLines = [];
					if (!data.navigateToEntity) {
						this.changeMode(channelId, eventId, data);
						this.reloadTable(channelId, eventId, data);
						oEvent.getSource().getParent().close();
					} else {
						sap.ui.getCore().getEventBus().publish("app", "proceedWithNavigation", {
							title : data.title,
							metadata : data.metadata
						});
						oEvent.getSource().getParent().close();
					}

				}.bind(this)
			}),
			endButton : new sap.m.Button({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_CANCEL'),
				press : function(oEvent) {
					this.getView().editSwitch.setState(this.editMode);
					oEvent.getSource().getParent().close();
				}.bind(this)
			}),
			afterClose : function() {
				dialog.destroy();
			}
		});
		dialog.open();
	},

	setEditable : function(channelId, eventId, data) {
		data.navigateToEntity = false;
		if (this.editMode && this.hasUnsavedChanges()) {
			this.showLossChangePopup(channelId, eventId, data);
		} else {
			this.changeMode(channelId, eventId, data);
		}
	},

	addRemoveFooterButtons : function(editMode) {
		this.getView().toolbar.removeAllContent();
		if (editMode) {
			this.getView().toolbar.addContent(new sap.m.Button({
				icon : "sap-icon://sys-add",
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_ADD'),
				press : function(oEvent) {
					this.createEntity(oEvent);
				}.bind(this)
			}));
			this.getView().toolbar.addContent(new sap.m.ToolbarSpacer());
			if (!this.saveButton) {
				this.saveButton = new sap.m.Button({
					icon : "sap-icon://save",
					text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SAVE'),
					press : function(oEvent) {
						this.saveButton.setEnabled(false);
						sap.ui.getCore().getEventBus().publish("savingEntries", "load");
						this.currentLineSaved = -1;
						this.saveEditedEntries(this.editedLines);
					}.bind(this)
				});
			}
			this.saveButton.setEnabled(false);
			this.getView().toolbar.addContent(this.saveButton);
			if (this.editedLines && this.editedLines.length > 0) {
				this.saveButton.setEnabled(true);
			}
			this.deleteButton = new sap.m.Button({
				icon : "sap-icon://delete",
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_DELETE'),
				press : function(oEvent) {
					this.onDeleteRequested();
				}.bind(this)
			});
			this.deleteButton.setEnabled(false);
			this.getView().toolbar.addContent(this.deleteButton);
		}
	},

	setPageTitle : function(title) {
		this.getView().page.setTitle(title);
	},

	buildContentDisplay : function(columns, keys, navigationColumns, data) {
		this.table = new sap.m.Table({
			growing : true,
			growingThreshold : 100,
			growingScrollToLoad : true
		});
		this.editMode ? this.table.setMode(sap.m.ListMode.MultiSelect) : this.table.setMode(sap.m.ListMode.None);
		this.addRemoveFooterButtons(this.editMode);

		this.table.attachSelectionChange({}, this.onItemSelected, this);

		var allColumns = columns;
		var navigationColumnsToAdd = this.getNavigationColumns(allColumns, navigationColumns);

		for ( var i in navigationColumnsToAdd) {
			allColumns.push(navigationColumnsToAdd[i]);
		}

		this.addKeyFlagInColumns(allColumns, keys);
		this.addNavigationFlagInColumns(allColumns, navigationColumns);

		// Put the key fields first
		allColumns.sortBy("-isKey", "name");
		allColumns = this.setRecordStatusFirst(allColumns);

		var viewSettingsDialog = this.buildViewSettingsDialog(columns, data);

		var tableToolbar = new sap.m.Toolbar({
			content : [
						new sap.m.Text({
							text : "# Entries found: " + data.length,
							width : "20%",
						}), new sap.m.ToolbarSpacer({
							width : "60%"
						}), new sap.m.SearchField({
							tooltip : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SEARCH'),
							width : "20%",
							search : function(oEvent) {
								this.filterTable(this.createFilterForTable(oEvent.getParameter("query"), columns));
							}.bind(this),
							liveChange : function(oEvent) {
								this.filterTable(this.createFilterForTable(oEvent.getParameter("newValue"), columns));
							}.bind(this)
						}), new sap.m.Button({
							icon : 'sap-icon://action-settings',
							press : function(oEvent) {
								viewSettingsDialog.open();
							}.bind(this)
						})
			]
		});

		this.table.setHeaderToolbar(tableToolbar);
		this.table.addStyleClass("sapUiSizeCompact");

		var oModel = new sap.ui.model.json.JSONModel({
			columns : allColumns,
			"rows" : data
		});
		this.table.setModel(oModel);

		this.editMode ? this.bindColumnsForEdit(this.table) : this.bindColumns(this.table);
		this.editMode ? this.bindRowsForEdit(this.table) : this.bindRows(this.table);

		this.getView().page.removeAllContent();
		this.getView().page.addContent(this.table);
	},

	bindColumns : function(table) {
		table.bindAggregation("columns", "/columns", function(index, context) {
			if (context.getObject().name !== "RecordStatus" && context.getObject().name !== "FieldStatus" && context.getObject().name !== "FieldType") {
				return new sap.m.Column({
					header : new sap.m.Label({
						text : context.getObject().name
					}),
					visible : true,
					styleClass : context.getObject().isKey ? "entityKey" : ""
				});
			} else {
				return new sap.m.Column({
					header : new sap.m.Label({
						text : ""
					}),
					visible : true,
					styleClass : context.getObject().isKey ? "entityKey" : "",
					tooltip : context.getObject().name,
					width : "0.8rem",
					hAlign : sap.ui.core.TextAlign.Center
				});
			}

		});
	},

	bindColumnsForEdit : function(table) {
		table.bindAggregation("columns", "/columns", function(index, context) {
			return new sap.m.Column({
				header : new sap.m.Label({
					text : (context.getObject().name == "TextsDetails" || context.getObject().name == "TextsMTDetails") ? sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_DESCR') : context.getObject().name
				}),
				visible : true,
				styleClass : context.getObject().isKey ? "entityKey" : ""
			});
		});
	},

	bindRows : function(table) {
		var thisInstance = this;
		table.bindItems("/rows", function(index, context) {
			// get the current data for the row
			var obj = context.getObject();
			// get the columns from the model
			var columns = context.getModel().oData.columns;
			// Apply the same sorting than for the generation of columns headers
			columns.sortBy("-isKey", "name");
			// Set the record status first
			columns = thisInstance.setRecordStatusFirst(columns);
			// Create the row element
			var row = new sap.m.ColumnListItem({
				vAlign : sap.ui.core.VerticalAlign.Middle,
				customData : [
					{
						key : 'object',
						value : obj
					}
				]
			});
			// For each column, create the cell
			for ( var k in columns) {
				// If the columns is a navigation
				if (columns[k].name === "TextsDetails" || columns[k].name === "TextsMTDetails") {
					// Get description in user language
					row.addCell(new sap.m.Text({
						text : thisInstance.getDescription(obj[columns[k].name].results),
						customData : [
									{
										key : 'object',
										value : obj
									}, {
										key : 'columnName',
										value : columns[k].name
									}, {
										key : 'toObject',
										value : columns[k].toRole
									}, {
										key : 'referenceToThis',
										value : thisInstance
									}
						]
					}).addStyleClass('spinClickableCell').attachBrowserEvent('click', thisInstance.cellNavClicked));
				} else if (columns[k].hasNav) {
					var columnText = obj[columns[k].name];
					if (columns[k].name.indexOf("Details") > -1) {
						columnText = sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SEE') + columns[k].name;
					}
					row.addCell(new sap.m.Text({
						text : columnText,
						customData : [
									{
										key : 'object',
										value : obj
									}, {
										key : 'columnName',
										value : columns[k].name
									}, {
										key : 'toObject',
										value : columns[k].navProp
									}, {
										key : 'referenceToThis',
										value : thisInstance
									}
						]
					}).addStyleClass('spinClickableCell').attachBrowserEvent('click', thisInstance.cellNavClicked));
					// For record status, show an icon
				} else if (columns[k].name == "RecordStatus") {
					if (obj[columns[k].name] == 'ACTIVE')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://status-positive",
							color : "green",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else if (obj[columns[k].name] == 'DELETED')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://status-negative",
							color : "red",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://question-mark",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					// For field status show an icon
				} else if (columns[k].name == "FieldStatus") {
					if (obj[columns[k].name] == 'ENABLED')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://status-inactive",
							color : "green",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else if (obj[columns[k].name] == 'DISABLED')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://status-inactive",
							color : "orange",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://question-mark",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					// For field type, show an icon
				} else if (columns[k].name == "FieldType") {
					if (obj[columns[k].name] == 'DATABASE')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://database",
							color : "orange",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else if (obj[columns[k].name] == 'CALCULATED')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://simulate",
							color : "orange",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else if (obj[columns[k].name] == 'CONSTANT')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://provision",
							color : "orange",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else if (obj[columns[k].name] == 'DATASOURCE')
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://puzzle",
							color : "orange",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					else
						row.addCell(new sap.ui.core.Icon({
							src : "sap-icon://question-mark",
							size : "0.8rem"
						}).setTooltip(obj[columns[k].name]));
					// For all other fields, show a text
				} else {
					row.addCell(new sap.m.Text({
						text : obj[columns[k].name]
					}));
				}
			}
			return row;
		});
	},

	bindRowsForEdit : function(table) {
		var thisInstance = this;
		table.bindItems("/rows", function(index, context) {
			// get the current data for the row
			var obj = context.getObject();

			var path = context.getPath().split("/");
			var indexInModel = path[(path.length) - 1];

			// get the columns from the model
			var columns = context.getModel().oData.columns;
			// Apply the same sorting than for the generation of columns headers
			columns.sortBy("-isKey", "name");
			// Set the record status first
			columns = thisInstance.setRecordStatusFirst(columns);
			// Create the row element
			var row = new sap.m.ColumnListItem({
				vAlign : sap.ui.core.VerticalAlign.Middle,
				customData : [
					{
						key : 'object',
						value : obj
					}
				]
			});
			// For each column, create the cell
			for ( var k in columns) {
				var colName = columns[k].name;
				// If the columns is a navigation
				if (columns[k].name == "TextsDetails" || columns[k].name == "TextsMTDetails") {
					// Get description in user language
					row.addCell(new sap.m.Text({
						text : thisInstance.getDescription(obj[columns[k].name].results),
						customData : [
									{
										key : 'object',
										value : obj
									}, {
										key : 'columnName',
										value : columns[k].name
									}, {
										key : 'subEntityName',
										value : columns[k].toRole
									}, {
										key : 'referenceToThis',
										value : thisInstance
									}
						]
					}).addStyleClass('spinClickableCell').attachBrowserEvent('click', thisInstance.cellDescriptionClicked));
					// for navigation properties, show a link to the actual
					// linked object
				} else if (columns[k].hasNav) {
					var columnText = obj[columns[k].name];
					if (columns[k].name.indexOf("Details") > -1) {
						columnText = sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_EDIT') + columns[k].name;

						row.addCell(new sap.m.Text({
							text : columnText,
							customData : [
										{
											key : 'object',
											value : obj
										}, {
											key : 'columnName',
											value : columns[k].name
										}// , {
										// key : 'toObject',
										// value : columns[k].navProp
										// }
										, {
											key : 'subEntityName',
											value : columns[k].toRole
										}, {
											key : 'referenceToThis',
											value : thisInstance
										}
							]
						}).addStyleClass('spinClickableCell').attachBrowserEvent('click', thisInstance.cellDescriptionClicked));
					} else {
						row.addCell(new sap.m.Input({
							value : obj[columns[k].name],
							customData : [
										{
											key : 'object',
											value : obj
										}, {
											key : 'columnName',
											value : columns[k].name
										}, {
											key : 'toObject',
											value : columns[k].navProp
										}, {
											key : 'referenceToThis',
											value : thisInstance
										}
							],
							liveChange : function(oEvent) {
								sap.ui.getCore().getEventBus().publish("app", "valueChanged", {
									oEvent : oEvent,
									indexInModel : indexInModel
								// index
								});
							}
						}));
					}
				} else if (columns[k].name == "Id") {
					row.addCell(new sap.m.Text({
						text : obj[columns[k].name]
					}));
				} else if (columns[k].name == "RecordStatus" || columns[k].name == "FieldStatus" || columns[k].name == "FieldType") {
					row.addCell(new sap.m.Input({
						showValueHelp : true,
						valueHelpOnly : true,
						value : obj[columns[k].name],
						customData : [
									{
										key : "fieldName",
										value : columns[k].name
									}, {
										key : "indexInModel",
										value : indexInModel
									// index
									}
						],
						valueHelpRequest : function(oEvent) {
							sap.ui.getCore().getEventBus().publish("app", "showValueHelp", {
								field : colName,
								input : this
							});
						}
					}));
				} else {
					row.addCell(new sap.m.Input({
						value : obj[columns[k].name],
						customData : [
									{
										key : 'object',
										value : obj
									}, {
										key : 'columnName',
										value : columns[k].name
									}
						],
						liveChange : function(oEvent) {
							sap.ui.getCore().getEventBus().publish("app", "valueChanged", {
								oEvent : oEvent,
								indexInModel : indexInModel
							// index
							});
						}
					}));
				}
			}
			return row;
		});
	},

	showValueHelp : function(channelId, eventId, data) {
		var valueModel = null;

		this.lastSelectedInput = data.input;

		if (this.lastSelectedInput.getCustomData()[0].getValue() === "RecordStatus") {
			valueModel = new sap.ui.model.json.JSONModel([
						{
							text : "ACTIVE",
							key : "ACTIVE"
						}, {
							text : "DELETED",
							key : "DELETED"
						}
			]);
		} else if (data.field === "FieldStatus") {
			valueModel = new sap.ui.model.json.JSONModel([
						{
							text : "ENABLED",
							key : "ENABLED"
						}, {
							text : "DISABLED",
							key : "DISABLED"
						}
			]);
		} else if (data.field === "FieldType") {
			valueModel = new sap.ui.model.json.JSONModel([
						{
							text : "DATABASE",
							key : "DATABASE"
						}, {
							text : "CALCULATED",
							key : "CALCULATED"
						}, {
							text : "CONSTANT",
							key : "CONSTANT"
						}, {
							text : "DATASOURCE",
							key : "DATASOURCE"
						}
			]);
		}

		var valueHelpDialog = sap.ui.xmlfragment("views.fragments.valueHelp", this);
		valueHelpDialog.setModel(valueModel);
		this.getView().addDependent(valueHelpDialog);
		valueHelpDialog.open();
	},

	handleValueHelpClose : function(oEvent) {
		this.lastSelectedInput.setValue(oEvent.getParameter("selectedItem").getTitle());
		this.valueSelected({
			indexInModel : this.lastSelectedInput.getCustomData()[1].getValue(),
			property : this.lastSelectedInput.getCustomData()[0].getValue(),
			newValue : oEvent.getParameter("selectedItem").getTitle()
		});
	},

	valueChanged : function(channelId, eventId, data) {
		var newValue = data.oEvent.getParameter('value');
		var property = data.oEvent.getSource().getCustomData()[1].getValue();
		var indexInModel = data.indexInModel;
		var tableData = this.table.getModel().getData();
		tableData.rows[indexInModel][property] = newValue;

		this.saveButton.setEnabled(true);

		// Check if all property are empty, if they are, the line is it
		// shouldn't be considered as edited.
		// In case it's editing an existing entry, the ID will always have a
		// value as it can't be changed

		if (!this.checkLineIsEmpty(tableData.rows[indexInModel], this.entityMetadata)) {
			if (this.editedLines.indexOf(indexInModel) === -1)
				this.editedLines.push(indexInModel);
		} else {
			var indexInEditedLines = this.editedLines.indexOf(indexInModel);
			if (indexInEditedLines !== -1) {
				this.editedLines.splice(indexInEditedLines, 1);
			}
			if (this.editedLines && this.editedLines.length > 0)
				this.saveButton.setEnabled(false);
		}
	},

	valueSelected : function(data) {
		var newValue = data.newValue;
		var property = data.property;
		var indexInModel = parseInt(data.indexInModel.replace(this.table.getId() + '-', ''));
		var tableData = this.table.getModel().getData();
		tableData.rows[indexInModel][property] = newValue;

		this.saveButton.setEnabled(true);

		// Check if all property are empty, if they are, the line is it
		// shouldn't be considered as edited.
		// In case it's editing an existing entry, the ID will always have a
		// value as it can't be changed

		if (!this.checkLineIsEmpty(tableData.rows[indexInModel], this.entityMetadata)) {
			if (this.editedLines.indexOf(indexInModel) === -1)
				this.editedLines.push(indexInModel);
		} else {
			var indexInEditedLines = this.editedLines.indexOf(indexInModel);
			if (indexInEditedLines !== -1) {
				this.editedLines.splice(indexInEditedLines, 1);
			}
			if (this.editedLines && this.editedLines.length > 0)
				this.saveButton.setEnabled(false);
		}
	},

	checkLineIsEmpty : function(rowToCheck, entityMetadata) {
		for ( var i in entityMetadata.property) {
			if (rowToCheck[entityMetadata.property[i].name] !== '')
				return false;
		}
		return true;
	},

	buildViewSettingsDialog : function(columns, values) {
		var dialog = new sap.m.ViewSettingsDialog({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_VIEWSETTINGS'),
			confirm : function(oEvent) {
				var sortItem = this.getSelectedSortItem();
				var sortColumns = this.getSortItems();
				var columnToSort = null;
				for ( var i in sortColumns) {
					if (sortColumns[i].getId() === sortItem) {
						columnToSort = sortColumns[i].getKey();
					}
				}
				var sortDescending = this.getSortDescending();

				var grouping = this.getSelectedGroupItem();
				var groupColumns = this.getGroupItems();
				var groupingToApply = null;
				for ( var i in groupColumns) {
					if (groupColumns[i].getId() === grouping) {
						groupingToApply = groupColumns[i].getKey();
					}
				}
				var groupDescending = this.getGroupDescending();

				var filters = this.getSelectedFilterItems();

				sap.ui.getCore().getEventBus().publish("app", "sortFilterValueList", {
					columnToSort : columnToSort,
					sortDescending : sortDescending,
					grouping : groupingToApply,
					groupDescending : groupDescending,
					filters : filters
				});
			},
			cancel : function(oEvent) {
				oEvent.getSource().getParent().close();
			},
			resetFilters : function(oEvent) {
			}
		});

		for ( var i in columns) {
			dialog.addSortItem(new sap.m.ViewSettingsItem({
				text : columns[i].name,
				key : columns[i].name
			}));

			dialog.addGroupItem(new sap.m.ViewSettingsItem({
				text : columns[i].name,
				key : columns[i].name
			}));

			var filterItem = new sap.m.ViewSettingsFilterItem({
				text : columns[i].name,
				key : columns[i].name
			});

			var uniqueValues = this.getUniqueValues(values, columns[i].name);
			uniqueValues.sort();

			for ( var j in uniqueValues) {
				filterItem.addItem(new sap.m.ViewSettingsItem({
					text : uniqueValues[j],
					key : columns[i].name
				}));
			}

			dialog.addFilterItem(filterItem);
		}
		return dialog;
	},

	createFilterForTable : function(value, columns) {
		var filters = [];
		for ( var i in columns) {
			if (columns[i].name.indexOf("Details") == -1 && columns[i].name !== "RecordStatus" && columns[i].name !== "FieldStatus" && columns[i].name !== "FieldType")
				filters.push(new sap.ui.model.Filter(columns[i].name, sap.ui.model.FilterOperator.Contains, value));
		}
		var finalFilter = new sap.ui.model.Filter({
			filters : filters,
			and : false
		});
		return finalFilter;
	},

	createSorterForTable : function(path, descending, group) {
		return new sap.ui.model.Sorter(path, descending, group);
	},

	filterTable : function(filters) {
		this.table.getBinding("items").filter(filters, sap.ui.model.FilterType.Application);
	},

	sortFilterValueList : function(channelId, eventId, data) {

		var aSorters = [];
		if (data.grouping) {
			aSorters.push(new sap.ui.model.Sorter(data.grouping, data.groupDescending, true));
		}
		if (data.columnToSort !== null) {
			aSorters.push(new sap.ui.model.Sorter(data.columnToSort, data.sortDescending));
		}
		this.table.getBinding("items").sort(aSorters);
		if (data.filters.length > 0) {
			var aStructuredFilters = this.buildStructuredFilters(data.filters);

			var filterForColumns = [];

			for ( var i in aStructuredFilters) {
				var filters = [];

				for ( var j in aStructuredFilters[i].values) {
					filters.push(new sap.ui.model.Filter(aStructuredFilters[i].column, sap.ui.model.FilterOperator.EQ, aStructuredFilters[i].values[j]));
				}

				filterForColumns.push(new sap.ui.model.Filter({
					filters : filters,
					and : false
				}));

			}

			var finalFilter = new sap.ui.model.Filter({
				filters : filterForColumns,
				and : true
			});

			this.table.getBinding("items").filter(finalFilter, sap.ui.model.FilterType.Application);

		} else {
			this.table.getBinding("items").filter([]);
		}
	},

	buildStructuredFilters : function(aFilters) {
		var aStructuredFilters = [];
		for ( var i in aFilters) {
			var index = null;
			if ((index = this.filterKeyIndexInFilterArray(aStructuredFilters, aFilters[i].getKey())) !== null) {
				aStructuredFilters[index].values.push(aFilters[i].getText());
			} else {
				aStructuredFilters.push({
					column : aFilters[i].getKey(),
					values : [
						aFilters[i].getText()
					]
				});
			}
		}
		return aStructuredFilters;
	},

	filterKeyIndexInFilterArray : function(aFilters, sFilterKey) {
		for ( var i in aFilters) {
			if (aFilters[i].column === sFilterKey) {
				return i;
			}
		}
		return null;
	},

	// ******************************************************************************************
	// * TRANSLATION MANAGMENT *
	// ******************************************************************************************
	translationTableBindColumns : function(table, isEdit) {
		table.bindAggregation("columns", "/columns", function(index, context) {
			return new sap.m.Column({
				header : new sap.m.Label({
					text : (context.getObject().name == "TextsDetails" || context.getObject().name == "TextsMTDetails") ? sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_DESCR') : context.getObject().name
				}),
				visible : true,
				styleClass : context.getObject().isKey ? "entityKey" : ""
			});
		});
	},

	translationTableBindRowsForEdit : function(table) {
		var thisInstance = this;
		table.bindItems("/rows", function(index, context) {
			// get the current data for the row
			var obj = context.getObject();
			// get the columns from the model
			var columns = context.getModel().oData.columns;
			// Apply the same sorting than for the generation of columns headers
			columns.sortBy("-isKey", "name");
			// Set the record status first
			columns = thisInstance.setRecordStatusFirst(columns);
			// Create the row element
			var row = new sap.m.ColumnListItem({
				vAlign : sap.ui.core.VerticalAlign.Middle,
				customData : [
					{
						key : 'object',
						value : obj
					}
				]
			});
			// For each column, create the cell
			for ( var k in columns) {
				if (columns[k].name == "Id") {
					row.addCell(new sap.m.Text({
						text : obj[columns[k].name]
					}));
				} else {
					row.addCell(new sap.m.Input({
						value : obj[columns[k].name],
						customData : [
									{
										key : 'object',
										value : obj
									}, {
										key : 'columnName',
										value : columns[k].name
									}
						],
						liveChange : function(oEvent) {
							sap.ui.getCore().getEventBus().publish("app", "translationValueChanged", {
								oEvent : oEvent,
								indexInModel : index,
								table : table
							});
						}
					}));
				}

			}
			return row;
		});
	},

	translationTableValueChanged : function(channelId, eventId, data) {
		var newValue = data.oEvent.getParameter('value');
		var property = data.oEvent.getSource().getCustomData()[1].getValue();
		var indexInModel = parseInt(data.indexInModel.replace(data.table.getId() + '-', ''));
		var tableData = data.table.getModel().getData();
		tableData.rows[indexInModel][property] = newValue;
	},

	translationTableRebuildBinding : function(channelId, eventId, data) {
		this.translationTableBindRowsForEdit(data.table);
	},

	translationTableSaveEntries : function(channelId, eventId, data) {
		this.translationToSave = [];
		for ( var row in data.table.getModel().getData().rows) {
			if (!this.translationTableCheckLineIsEmpty(data.table.getModel().getData().rows[row], data.table.getModel().getData().columns)) {
				this.translationToSave.push({
					entity : data.entity.replace("Details", ""),
					data : data.table.getModel().getData().rows[row],
					columns : data.table.getModel().getData().columns
				});
			}
		}
		this.currentSavedTranslation = -1;
		this.translationSaveInDB();
	},

	translationSaveInDB : function() {
		this.currentSavedTranslation++;
		var payload = this.translationBuildPayload(this.translationToSave[this.currentSavedTranslation].data, this.translationToSave[this.currentSavedTranslation].columns);
		var content = {};
		content.oData = payload;
		if (this.translationToSave[this.currentSavedTranslation].data.Id === '') {
			content.serviceType = 'translationDataCreated';
			model.DataService.createData(content, (this.translationToSave[this.currentSavedTranslation].entity + "s"), null, []);
		} else {
			var keyParams = "(" + this.translationToSave[this.currentSavedTranslation].data.Id + ")";
			content.serviceType = 'translationDataUpdated';
			var url = this.translationToSave[this.currentSavedTranslation].entity + "s" + keyParams;
			model.DataService.updateData(content, url, null, []);
		}

	},

	translationBuildPayload : function(data, columns) {
		var payload = {};
		for ( var i in columns) {
			if (columns[i].name !== 'Id') {
				if (columns[i].hasNav) {
					if (data[columns[i].name] !== null && data[columns[i].name] !== "") {
						if (columns[i].name === "TextsMTDetails" && columns[i].name === "TextsDetails") {
							payload[columns[i].name] = {
								"__metadata" : {
									"id" : columns[i].navProp + 's' + "(" + data[columns[i].name] + "L)",
									"uri" : columns[i].navProp + 's' + "(" + data[columns[i].name] + "L)"
								}
							};
						} else {
							payload[columns[i].name + "Details"] = {
								"__metadata" : {
									"id" : columns[i].navProp + 's' + "(" + data[columns[i].name] + "L)",
									"uri" : columns[i].navProp + 's' + "(" + data[columns[i].name] + "L)"
								}
							};
						}

					} else {
						payload[columns[i].name] = null;
					}
				} else {
					payload[columns[i].name] = data[columns[i].name];
				}

			}
		}
		return payload;
	},

	translationTableCheckLineIsEmpty : function(row, columns) {
		for ( var i in columns) {
			if (row[columns[i].name] !== '')
				return false;
		}
		return true;
	},

	translationDataCreated : function(channelId, eventId, data) {
		if (this.currentTranslationEdit.object[this.currentTranslationEdit.property] === "")
			this.currentTranslationEdit.object[this.currentTranslationEdit.property] = {
				results : []
			};
		if (this.currentSavedTranslation < (this.translationToSave.length - 1)) {
			if (this.currentTranslationEdit.object[this.currentTranslationEdit.property].results[this.currentSavedTranslation])
				this.currentTranslationEdit.object[this.currentTranslationEdit.property].results[this.currentSavedTranslation] = data.dataObject;
			else
				this.currentTranslationEdit.object[this.currentTranslationEdit.property].results.push(data.dataObject);
			this.translationSaveInDB();
		} else {
			if (this.currentTranslationEdit.object[this.currentTranslationEdit.property].results[this.currentSavedTranslation])
				this.currentTranslationEdit.object[this.currentTranslationEdit.property].results[this.currentSavedTranslation] = data.dataObject;
			else
				this.currentTranslationEdit.object[this.currentTranslationEdit.property].results.push(data.dataObject);
			this.setCurrentTranslationModified();
			this.translationPopup.close();
		}
	},

	translationDataCreatedError : function(channelId, eventId, data) {
		if (this.currentSavedTranslation < (this.translationToSave.length - 1)) {
			this.translationSaveInDB();
		} else {

		}
	},

	translationDataUpdated : function(channelId, eventId, data) {
		if (this.currentSavedTranslation < (this.translationToSave.length - 1)) {
			this.translationSaveInDB();
		} else {
			this.setCurrentTranslationModified();
			this.translationPopup.close();
		}
	},

	translationDataUpdatedError : function(channelId, eventId, data) {
		if (this.currentSavedTranslation < (this.translationToSave.length - 1)) {
			this.translationSaveInDB();
		} else {

		}
	},

	translationTableDeleteEntries : function(channelId, eventId, data) {
		this.translationTableSelectedItemsToDelete = data.table.getSelectedItems();
		this.lastTranslationDeletedEntry = -1;
		this.translationTableDeleteSelectedEntry();
	},

	translationTableDeleteSelectedEntry : function() {
		this.lastTranslationDeletedEntry++;
		var object = this.translationTableSelectedItemsToDelete[this.lastTranslationDeletedEntry].getCustomData()[0].getValue();
		// new line
		if (object.Id === "") {
			this.translationTableEntityDataDeleted("app");
			// existing entry
		} else {
			debugger;
			var keyParams = "(" + object.Id + ")";
			var content = {};
			content.serviceType = 'translationTableEntityDataDeleted';
			var url = (this.currentTranslationEdit.property.replace("Details", "")) + 's' + keyParams;
			model.DataService.deleteData(content, url, null, []);
		}
	},

	translationTableEntityDataDeleted : function(channelId, eventId, data) {
		if (this.lastTranslationDeletedEntry < (this.translationTableSelectedItemsToDelete.length - 1)) {
			if (this.currentTranslationEdit.object[this.currentTranslationEdit.property].results) {
				var indexOfEntry = this.translationTableGetIndexInTranslationModel();
				if (indexOfEntry !== -1)
					this.currentTranslationEdit.object[this.currentTranslationEdit.property].results.splice(indexOfEntry, 1);
			}
			this.translationTableDeleteSelectedEntry();
		} else {
			this.setCurrentTranslationModified();
			this.translationPopup.close();
		}
	},

	translationTableEntityDataDeletedError : function(channelId, eventId, data) {
		if (this.lastTranslationDeletedEntry < (this.translationTableSelectedItemsToDelete.length - 1)) {
			this.translationTableDeleteSelectedEntry();
		} else {
			this.setCurrentTranslationModified();
			this.translationPopup.close();
		}
	},

	translationTableGetIndexInTranslationModel : function() {
		for ( var i in this.currentTranslationEdit.object[this.currentTranslationEdit.property].results) {
			if (this.currentTranslationEdit.object[this.currentTranslationEdit.property].results[i] === this.translationTableSelectedItemsToDelete[this.lastTranslationDeletedEntry].getCustomData()[0].getValue().Id)
				return i;
		}
		return -1;
	},

	setCurrentTranslationModified : function() {
		var indexInTable = null;
		var rows = this.table.getModel().getData().rows;
		for ( var i in rows) {
			if (rows[i].Id === this.currentTranslationEdit.object.Id) {
				indexInTable = i;
				break;
			}
		}
		this.saveButton.setEnabled(true);
		if (this.editedLines.indexOf(indexInTable) < 0)
			this.editedLines.push(indexInTable);
	},

	// ******************************************************************************************
	// * CRUD ACTIONS *
	// ******************************************************************************************
	createEntity : function(oEvent) {
		var tableData = this.table.getModel().getData();
		var newLine = {};
		for ( var i in this.entityMetadata.property) {
			newLine[this.entityMetadata.property[i].name] = '';
		}
		tableData.rows.push(newLine);
		this.table.unbindItems();
		this.bindRowsForEdit(this.table);
	},

	saveEditedEntries : function(editedLines) {
		this.currentLineSaved++;
		var tableData = this.table.getModel().getData();
		var payload = this.buildPayload(tableData.rows[editedLines[this.currentLineSaved]]);
		var content = {};
		content.oData = payload;
		if (tableData.rows[editedLines[this.currentLineSaved]].Id === '') {
			content.serviceType = 'entityDataCreated';
			model.DataService.createData(content, (this.entityMetadata.name + 's'), null, []);
		} else {
			var keyParams = this.buildKeyString(tableData.rows[editedLines[this.currentLineSaved]]);
			content.serviceType = 'entityDataUpdated';
			var url = this.entityMetadata.name + 's' + keyParams;
			model.DataService.updateData(content, url, null, []);
		}
	},

	buildPayload : function(modelValues) {
		var payload = {};
		for ( var i in this.entityMetadata.property) {
			if (this.entityMetadata.property[i].name !== 'Id') {
				if (this.entityMetadata.property[i].hasNav) {
					if (modelValues[this.entityMetadata.property[i].name].results) {
						var metadata = [];
						for ( var j in modelValues[this.entityMetadata.property[i].name].results) {
							metadata.push({
								"__metadata" : {
									"id" : this.entityMetadata.property[i].navProp + 's' + "(" + modelValues[this.entityMetadata.property[i].name].results[j].Id + "L)",
									"uri" : this.entityMetadata.property[i].navProp + 's' + "(" + modelValues[this.entityMetadata.property[i].name].results[j].Id + "L)"
								}
							});
						}
						payload[this.entityMetadata.property[i].name] = metadata;
					} else {
						if (modelValues[this.entityMetadata.property[i].name] !== "") {
							if(modelValues[this.entityMetadata.property[i].name].__deferred){
								continue;
							}
							payload[this.entityMetadata.property[i].name + 'Details'] = {
								"__metadata" : {
									"id" : this.entityMetadata.property[i].navProp + 's' + "(" + modelValues[this.entityMetadata.property[i].name] + "L)",
									"uri" : this.entityMetadata.property[i].navProp + 's' + "(" + modelValues[this.entityMetadata.property[i].name] + "L)"
								}
							};
						}
					}

				} else {
					payload[this.entityMetadata.property[i].name] = modelValues[this.entityMetadata.property[i].name];
				}
			}
		}
		return payload;
	},

	buildKeyString : function(rowData) {
		var keyParams = '(';
		// For each key fields of the entity
		for ( var i in this.entityMetadata.key.propertyRef) {
			var value = rowData[this.entityMetadata.key.propertyRef[i].name];
			var type = this.getPropertyType(this.entityMetadata.key.propertyRef[i].name);

			if (type === "Edm.Int64") {
				value = parseInt(value);
			}
			keyParams += this.entityMetadata.key.propertyRef[i].name + '=' + value + ',';
		}
		keyParams = keyParams.substring(0, keyParams.length - 1) + ')';

		return keyParams;
	},

	entityDataCreated : function(channelId, eventId, data) {
		console.log("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Created...");
		this.saveLog.push("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Created...");
		if (this.currentLineSaved < (this.editedLines.length - 1)) {
			this.saveEditedEntries(this.editedLines);
		} else {
			this.saveButton.setEnabled(true);
			sap.ui.getCore().getEventBus().publish("savingEntries", "loaded");
			this.showSaveLog();
		}

	},

	entityDataCreatedError : function(channelId, eventId, data) {
		console.log("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Error during creation...");
		this.saveLog.push("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Error during creation...");
		if (this.currentLineSaved < (this.editedLines.length - 1)) {
			this.saveEditedEntries(this.editedLines);
		} else {
			this.saveButton.setEnabled(true);
			sap.ui.getCore().getEventBus().publish("savingEntries", "loaded");
			this.showSaveLog();
		}
	},

	entityDataUpdated : function(channelId, eventId, data) {
		console.log("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Updated...");
		this.saveLog.push("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Updated...");
		if (this.currentLineSaved < (this.editedLines.length - 1)) {
			this.saveEditedEntries(this.editedLines);
		} else {
			this.saveButton.setEnabled(true);
			sap.ui.getCore().getEventBus().publish("savingEntries", "loaded");
			this.showSaveLog();
		}

	},

	entityDataUpdatedError : function(channelId, eventId, data) {
		console.log("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Error during update...");
		this.saveLog.push("Line " + (this.editedLines[this.currentLineSaved] + 1) + ": Error during update...");
		if (this.currentLineSaved < (this.editedLines.length - 1)) {
			this.saveEditedEntries(this.editedLines);
		} else {
			this.saveButton.setEnabled(true);
			sap.ui.getCore().getEventBus().publish("savingEntries", "loaded");
			this.showSaveLog();
		}
	},

	entityDataDeleted : function(channelId, eventId, data) {
		if (channelId === 'data')
			this.saveLog.push("Entry with id " + this.selectedItemsToDelete[this.lastDeletedEntry].getCustomData()[0].getValue().Id + " deleted...");
		if (this.lastDeletedEntry < (this.selectedItemsToDelete.length - 1)) {
			this.deleteSelectedEntry();
		} else {
			this.deleteButton.setEnabled(false);
			if (this.saveLog.length > 0)
				this.showSaveLog();
			else {
				this.editedLines = [];
				this.saveLog = [];
				this.selectedItemsToDelete = [];
				sap.ui.getCore().getEventBus().publish("app", "reloadTable");
			}
		}
	},

	entityDataDeletedError : function(channelId, eventId, data) {
		if (channelId === 'err')
			this.saveLog.push("The entry with id " + this.selectedItemsToDelete[this.lastDeletedEntry].getCustomData()[0].getValue().Id + " could not be deleted...");
		if (this.lastDeletedEntry < (this.selectedItemsToDelete.length - 1)) {
			this.deleteSelectedEntry();
		} else {
			this.deleteButton.setEnabled(false);
			this.showSaveLog();
		}
	},

	showSaveLog : function() {
		var content = new sap.m.VBox();
		for ( var i in this.saveLog) {
			content.addItem(new sap.m.Text({
				text : this.saveLog[i]
			}));
		}

		var dialog = new sap.m.Dialog({
			title : 'Save Log',
			type : 'Message',
			content : content,
			beginButton : new sap.m.Button({
				text : 'OK',
				press : function(oEvent) {
					this.editedLines = [];
					this.saveLog = [];
					this.selectedItemsToDelete = [];
					oEvent.getSource().getParent().close();
				}.bind(this)
			}),
			afterClose : function() {
				sap.ui.getCore().getEventBus().publish("app", "reloadTable");
				dialog.destroy();
			}
		});
		dialog.open();
	},

	onItemSelected : function(oEvent) {
		if (this.table.getSelectedItems().length > 0)
			this.deleteButton.setEnabled(true);
		else
			this.deleteButton.setEnabled(false);
	},

	onDeleteRequested : function() {
		if (this.editedLines.length > 0) {
			this.showSaveBeforeDeletePopup();
		} else {
			this.selectedItemsToDelete = this.table.getSelectedItems();
			this.lastDeletedEntry = -1;
			this.deleteSelectedEntry();
		}
	},

	deleteSelectedEntry : function() {
		this.lastDeletedEntry++;
		var object = this.selectedItemsToDelete[this.lastDeletedEntry].getCustomData()[0].getValue();
		// new line
		if (object.Id === "") {
			this.entityDataDeleted("app");
			// existing entry
		} else {
			var keyParams = this.buildKeyString(object);
			var content = {};
			content.serviceType = 'entityDataDeleted';
			var url = this.entityMetadata.name + 's' + keyParams;
			model.DataService.deleteData(content, url, null, []);
		}
	},

	cellDescriptionClicked : function(oEvent) {
		var object = this.getCustomData()[0].getValue();
		var property = this.getCustomData()[1].getValue();
		var toObject = this.getCustomData()[2].getValue();
		var thisInstance = this.getCustomData()[3].getValue();
		var translationValues = object[property].results;

		var isEdit = null;
		if(property !== "TextsDetails" && property !== "TextsMTDetails"){
			isEdit = "edit";
		}
		
		if (translationValues) {
			thisInstance.buildPopupForEdit(object, property, toObject, translationValues, isEdit);
		} else {
			// Do the expand first to get the data
			thisInstance.getExpandedValue(property, object.Id, toObject, "edit", object, property);
		}
	},

	buildPopupForEdit : function(object, property, toObject, translationValues, isEdit) {
		this.currentTranslationEdit = {
			object : object,
			property : property
		};
		var subTable = new sap.m.Table();
		subTable.setMode(sap.m.ListMode.MultiSelect);

		var subEntityMetadata = this.getEntityFullMetadata(toObject);

		this.translationColumns = subEntityMetadata.property; // this.getEntityMetadata(toObject);
		var navigationColumnsToAdd = this.getNavigationColumns(this.translationColumns, subEntityMetadata.navigationProperty);
		var keys = this.getEntityKeys(toObject);

		for ( var i in navigationColumnsToAdd) {
			if (navigationColumnsToAdd[i].name === "TextsMTDetails" || navigationColumnsToAdd[i].name === "TextsDetails")
				continue;
			this.translationColumns.push(navigationColumnsToAdd[i]);
		}
		
		if(isEdit === "edit"){
			this.translationColumns = this.removeTextColumns(this.translationColumns);
		}

		this.addKeyFlagInColumns(this.translationColumns, keys);
		this.addNavigationFlagInColumns(this.translationColumns, subEntityMetadata.navigationProperty);

		// this.addKeyFlagInColumns(this.translationColumns, keys);
		// Put the key fields first
		this.translationColumns.sortBy("-isKey", "name");

		var oModel = new sap.ui.model.json.JSONModel({
			columns : this.translationColumns,
			"rows" : translationValues
		});
		subTable.setModel(oModel);

		this.translationTableBindColumns(subTable, isEdit);
		this.translationTableBindRowsForEdit(subTable);

		this.translationPopup = new sap.m.Dialog({
			title : property,
			content : subTable,
			contentWidth : "400rem",
			buttons : [
						new sap.m.Button({
							icon : "sap-icon://sys-add",
							text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_ADD'),
							press : function(oEvent) {
								var tableModel = this.getParent().getParent().getContent()[0].getModel().getData();
								var newEntry = {};
								for ( var i in tableModel.columns) {
									newEntry[tableModel.columns[i].name] = "";
								}
								if (!tableModel.rows)
									tableModel.rows = [];
								tableModel.rows.push(newEntry);
								sap.ui.getCore().getEventBus().publish("app", "translationTableRebuildBinding", {
									table : this.getParent().getParent().getContent()[0]
								});
							}
						}), new sap.m.Button({
							icon : "sap-icon://delete",
							text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_DELETE'),
							press : function(oEvent) {
								sap.ui.getCore().getEventBus().publish("app", "translationTableDeleteEntries", {
									table : this.getParent().getParent().getContent()[0]
								});
								// oEvent.getSource().getParent().close();
							}
						}), new sap.m.Button({
							icon : "sap-icon://save",
							text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SAVE'),
							press : function(oEvent) {
								sap.ui.getCore().getEventBus().publish("app", "translationTableSaveEntries", {
									entity : this.getParent().getParent().getTitle(),
									table : this.getParent().getParent().getContent()[0]
								});
							}
						}),
			],

		}).addStyleClass("sapUiSizeCompact").open();

	},

	cellNavClicked : function(oEvent) {
		var object = this.getCustomData()[0].getValue();
		var property = this.getCustomData()[1].getValue();
		var toObject = this.getCustomData()[2].getValue();
		var thisInstance = this.getCustomData()[3].getValue();

		var filterValue = "";
		// Case 1: expand required before navigation
		if (object[property].__deferred) {
			//end
			thisInstance.getExpandedValue(property, object.Id, toObject, "nav");
			// Case 2: It's a direct navigation with Id provided
		} else {
			filterValue = "$filter=";
			if (object[property].results) {
				for ( var i in object[property].results) {
					if (parseInt(object[property].results[i].Id, 10) > -1)
						filterValue += "Id+eq+" + object[property].results[i].Id + "+or+";
				}
				filterValue = filterValue.substring(0, filterValue.length - 4);

			} else if (typeof (object[property]) === "object") {
				if (parseInt(object[property].Id, 10) > -1)
					filterValue += "Id+eq+" + object[property].Id;
			} else {
				filterValue += "Id+eq+" + object[property];
			}
			// Select the corresponding entity in the worklist
			sap.ui.getCore().getEventBus().publish("app", "listObjectSelected", {
				toSelect : toObject
			});
			// Load the data
			sap.ui.getCore().getEventBus().publish("app", "loadEntityContent", {
				title : toObject,
				metadata : thisInstance.getEntityFullMetadata(toObject),
				filter : filterValue
			});
		}
	},

	showSaveBeforeDeletePopup : function() {
		new sap.m.Dialog({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SAVEENTRIESTITLE'),
			content : new sap.m.Text({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_SAVEENTRIESMESSAGE')
			}),
			state : sap.ui.core.ValueState.Warning,
			beginButton : new sap.m.Button({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_OK'),
				press : function(oEvent) {
					oEvent.getSource().getParent().close();
				}
			}),
		}).addStyleClass("sapUiSizeCompact").open();
	},
	// ******************************************************************************************
	// * ATTRIBUTES MANIPULATIONS *
	// ******************************************************************************************
	addKeyFlagInColumns : function(columns, keys) {
		for ( var i in columns) {
			columns[i].isKey = this.isKey(columns[i], keys);
		}
	},

	isKey : function(column, keys) {
		for ( var i in keys) {
			if (keys[i].name == column.name)
				return true;
		}
		return false;
	},

	addNavigationFlagInColumns : function(allColumns, navigationColumns) {
		for ( var i in navigationColumns) {
			for ( var j in allColumns) {
				if (allColumns[j].relationship === navigationColumns[i].relationship || allColumns[j].name == navigationColumns[i].toRole) {
					allColumns[j].hasNav = true;
					allColumns[j].navProp = navigationColumns[i].toRole;
					break;
				}
			}
		}
	},

	getPropertyType : function(property) {
		for ( var i in this.entityMetadata.property) {
			if (this.entityMetadata.property[i].name == property) {
				return this.entityMetadata.property[i].type;
			}
			return null;
		}
	},

	isNavigationProperty : function(name) {
		for ( var i in this.entityMetadata.navigationProperty) {
			if (this.entityMetadata.navigationProperty[i].name == name)
				return true;
		}
		return false;
	},

	getEntityExpands : function() {
		var expands = "";
		for ( var i in this.entityMetadata.navigationProperty) {
			if (this.entityMetadata.navigationProperty[i].name === "TextsDetails" || this.entityMetadata.navigationProperty[i].name === "TextsMTDetails")
				expands += this.entityMetadata.navigationProperty[i].name + ',';
		}
		expands = expands.substring(0, expands.length - 1);
		return expands;
	},

	getDescription : function(descrTable) {
		var userLanguage = sap.ui.getCore().getConfiguration().getLanguage();
		var descriptionUL = "";
		var descriptionEN = "";
		for ( var i in descrTable) {
			if (descrTable[i].Language.toUpperCase() == userLanguage.toUpperCase())
				descriptionUL = descrTable[i].Description;
			else if (descrTable[i].Language.toUpperCase() == 'en'.toUpperCase())
				descriptionEN = descrTable[i].Description;
		}
		if (descriptionUL !== "")
			return descriptionUL;
		else if (descriptionEN !== "")
			return descriptionEN;
		else
			return sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_NOSUITDESCR');
	},

	getEntityMetadata : function(entityName) {
		for ( var i in model.DataService.getMetadata().entityType) {
			if (model.DataService.getMetadata().entityType[i].name == entityName)
				return model.DataService.getMetadata().entityType[i].property;
		}
		return null;
	},

	getEntityFullMetadata : function(entityName) {
		for ( var i in model.DataService.getMetadata().entityType) {
			if (model.DataService.getMetadata().entityType[i].name == entityName)
				return model.DataService.getMetadata().entityType[i];
		}
		return null;
	},

	getEntityKeys : function(entityName) {
		for ( var i in model.DataService.getMetadata().entityType) {
			if (model.DataService.getMetadata().entityType[i].name == entityName)
				return model.DataService.getMetadata().entityType[i].key.propertyRef;
		}
		return null;
	},

	getDescriptionColumn : function(navigationProperties) {
		for ( var i in navigationProperties) {
			if (navigationProperties[i].name === 'TextsDetails') {
				return {
					index : i,
					name : navigationProperties[i].name
				};
			} else if (navigationProperties[i].name === 'TextsMTDetails') {
				return {
					index : i,
					name : navigationProperties[i].name
				};
			}
		}
		return null;
	},

	getSettingsColumn : function(navigationProperties) {
		for ( var i in navigationProperties) {
			if (navigationProperties[i].name === 'SettingsMTDetails') {
				return {
					index : i,
					name : navigationProperties[i].name
				};
			}
		}
		return null;
	},

	hasDescriptionColumns : function(allColumns, text) {
		for ( var i in allColumns) {
			if (allColumns[i].name === text.name) {
				return true;
			}
		}
		return false;
	},

	hasSettingsColumns : function(allColumns, text) {
		for ( var i in allColumns) {
			if (allColumns[i].name === text.name) {
				return true;
			}
		}
		return false;
	},

	setRecordStatusFirst : function(columns) {
		var columnsCopy = [];
		for ( var i in columns) {
			if (columns[i].name == "RecordStatus") {
				columnsCopy.push(columns[i]);
				break;
			}
		}
		for ( var i in columns) {
			if (columns[i].name !== "RecordStatus") {
				columnsCopy.push(columns[i]);
			}
		}
		return columnsCopy;
	},

	getUniqueValues : function(values, propertyName) {
		var n = {}, r = [];
		for (var i = 0; i < values.length; i++) {
			var currentValue = values[i][propertyName];
			if (propertyName == "TextsDetails" || propertyName == "TextsMTDetails")
				currentValue = this.getDescription(currentValue.results);

			if (!n[currentValue]) {
				n[currentValue] = true;
				r.push(currentValue);
			}
		}
		return r;
	},

	getPropertyType : function(edmType) {
		switch (edmType) {
		case "Edm.Int64":
			return sap.m.InputType.Number;
			break;
		default:
			return sap.m.InputType.Text;
			break;
		}
	},

	getNavigationColumns : function(allColumns, navigationColumns) {
		var navColumns = [];
		for ( var i in navigationColumns) {
			var found = false;
			for ( var j in allColumns) {
				if (allColumns[j].name.toUpperCase() === navigationColumns[i].name.replace("Details", "").toUpperCase()) {
					found = true;
					allColumns[j].hasNav = true;
					allColumns[j].navProp = navigationColumns[i].toRole;
					break;
				}
			}
			if (!found && !navigationColumns[i].addedInDisplay) {
				navColumns.push(navigationColumns[i]);
				navigationColumns[i].addedInDisplay = true;
			}
		}
		return navColumns;
	},
	
	removeTextColumns:function(columns){
		var tempArray = [];
		for(var i in columns){
			if(columns[i].name !== "TextsDetails" && columns[i].name !== "TextsMTDetails"){
				tempArray.push(columns[i]);
			}
		}
		return tempArray;
	}
});
