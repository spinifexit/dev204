sap.ui.controller("views.worklist.worklist", {

	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf views.worklist
	 */
	onInit : function() {
		this.lastSelectedItem = null;
		// Define template for the list		
		this.oItemTemplate = new sap.m.StandardListItem({
			title : "{text}",
			icon : "{icon}",
			tooltip : "{text}",
			type : sap.m.ListType.Navigation,
			customData : [
				new sap.ui.core.CustomData({
					key : "entityDef",
					value : "{entityDef}"
				})
			]
		}).addStyleClass("spinTransparent").addStyleClass("sapUiSizeCompact");
		// Event listeners for global events
		var thisInstance = this;
		
		sap.ui.getCore().getEventBus().subscribe("data", "entityWorklistLoaded", thisInstance.entityListLoadedSuccess, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "entityWorklistLoaded", thisInstance.entityListLoadedError, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("err", "communicationFailure", thisInstance.entityListLoadedError, thisInstance);
		// Internal to app events
		sap.ui.getCore().getEventBus().subscribe("app", "listObjectSelected", thisInstance.deselectItem, thisInstance);
		sap.ui.getCore().getEventBus().subscribe("app", "proceedWithNavigation", thisInstance.navigateToSelectedItem , thisInstance);
	},
	
	goBack:function(oEvent){
		var globalNavigationModel = sap.ui.getCore().getModel("globalNavigationModel").getData();
		var navInfo = globalNavigationModel.pop();
		if(navInfo.from === "id.views.launch.LaunchPage"){
			sap.ui.getCore().getEventBus().publish("navApp", "back");
		}
	},

	onBeforeShow : function(oEvent) {
		// Raise the event to show the loading popup
		sap.ui.getCore().getEventBus().publish("loadingEntityList", "load");
		// get the entities from the metadata model
		var entities = model.DataService.getMetadata().entityType;
		// Show the error if no entities found
		if(entities == null){
			this.entityListLoadedError();
			return;
		}
		// Create the JSON Model content
		this.entityList = [];
		for ( var i in entities) {
			this.entityList.push({
				text : entities[i].name,
				icon : "sap-icon://database",
				entityDef: entities[i]
			});
		}
		// Create the JSON model itself
		this.entitiesModel = new sap.ui.model.json.JSONModel({
			entityList : this.entityList,
		});
		// Assign the model to the entity list
		this.getView().oEntiyList.setModel(this.entitiesModel);
		// Bind the display to the model
		this.bindList([]);
		// Raise the event that the entities are loaded
		sap.ui.getCore().getEventBus().publish("loadingEntityList", "loaded");
	},

	entityListLoadedSuccess : function(channelId, eventId, data) { 
		sap.ui.getCore().getEventBus().publish("loadingEntityList", "loaded");
	},

	entityListLoadedError : function(channelId, eventId, data) {
		sap.ui.getCore().getEventBus().publish("loadingEntityList", "loaded");

		new sap.m.Dialog({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_COMMERROR'),
			type : "Message",
			content : new sap.m.Text({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_LOADERROR', [
					sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_ENTITYLIST')
				])
			}),
			beginButton : new sap.m.Button({
				text : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_OK'),
				press : function(oEvent) {
					oEvent.getSource().getParent().close();
				}
			}),
			state : "Error"
		}).open();
	},

	bindList : function(filters) {
		this.getView().oEntiyList.unbindItems();
		this.getView().oEntiyList.detachSelectionChange(this.onSelectItem, this);
		this.getView().oEntiyList.setBusy(true);
		this.getView().oEntiyList.bindItems("/entityList", this.oItemTemplate);
		this.getView().oEntiyList.getBinding("items").filter(filters);
		this.getView().oEntiyList.setBusy(false);
		this.getView().oEntiyList.attachSelectionChange(this.onSelectItem, this);
	},

	filterList : function(like) {
		if (like && like.length > 0) {
			var oTextFilter = new sap.ui.model.Filter("text", sap.ui.model.FilterOperator.Contains, like);
			this.getView().oEntiyList.getBinding("items").filter([
				oTextFilter
			]);
		} else {
			this.bindList([]);
		}
	},

	onSelectItem : function(oEvent) {
		sap.ui.getCore().getEventBus().publish("app", "checkEditedValues", {
			title : oEvent.getParameter('listItem').getTitle(),
			metadata: oEvent.getParameter('listItem').getCustomData()[0].getValue()
		});
	},
	
	navigateToSelectedItem:function(channelId, eventId, data){
		sap.ui.getCore().getEventBus().publish("app", "loadEntityContent", {
			title : data.title,
			metadata: data.metadata,
			filter: ""
		});
	},
	
	deselectItem:function(channelId, eventId, data) {
		var itemToSelect = this.getListItemRef(data.toSelect);
		this.getView().oEntiyList.setSelectedItem(this.getView().oEntiyList.getSelectedItem(), false);
		if(itemToSelect !== null){
			this.getView().oEntiyList.setSelectedItem(itemToSelect, true);
		}
	},
	
	getListItemRef:function(itemTitle){
		var items = this.getView().oEntiyList.getItems();
		for(var i in items){
			if(items[i].getTitle() === itemTitle){
				return items[i];
			}
		}
		return null;
	}

});