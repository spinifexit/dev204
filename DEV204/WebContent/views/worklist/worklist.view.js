sap.ui.jsview("views.worklist.worklist", {

	/** Specifies the Controller belonging to this View. 
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf views.oEntiyList
	 */
	getControllerName : function() {
		return "views.worklist.worklist";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	 * Since the Controller is given to this method, its event handlers can be attached right away. 
	 * @memberOf views.oEntiyList
	 */
	createContent : function(oController) {

		this.page = new sap.m.Page({
			title : sap.ui.getCore().getModel('i18n').getResourceBundle().getText('TXT_ENTITIES'),
			showNavButton:true,
			navButtonPress: oController.goBack
		});

		this.page.setEnableScrolling(true);

		this.toolbar = new sap.m.Toolbar();
		
		this.page.setFooter(this.toolbar);
		
		this.oEntiyList = new sap.m.List();
		this.oEntiyList.setBusy(true);
		this.oEntiyList.bindProperty("noDataText", "{i18n>TXT_NOENTITIES}");
		this.oEntiyList.setInset(false);
		this.oEntiyList.setMode(sap.m.ListMode.SingleSelectMaster);
		this.oEntiyList.addStyleClass("spinTransparent").addStyleClass("sapUiSizeCompact");

		this.oSearchfield = new sap.m.SearchField({
			tooltip : '{i18n>TXT_SEARCH}',
			search : function(oEvent) {
				oController.filterList(oEvent.getParameter("query"));
			},
			liveChange : function(oEvent) {
				oController.filterList(oEvent.getParameter("newValue"));
			}
		}).addStyleClass("sapUiSizeCompact");
		this.page.addContent(this.oSearchfield);
		this.page.addContent(this.oEntiyList);
		
		this.page.addStyleClass("spinTransparent").addStyleClass("sapUiSizeCompact");
		
		return this.page;
	},

	onBeforeShow : function(oEvent) {
		this.getController().onBeforeShow(oEvent);
	}

});