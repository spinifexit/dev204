/**
 * @author Jonathan Jouret
 * @fileOverview This file define the data model used by the Easy report
 *               application
 * @version 0.1
 * @requires jquery.sap.storage
 */
jQuery.sap.declare("model.DataService");

model.DataService = {};
/**
 * @param serviceId
 * @returns {String}
 * @lends model.DataService.prototype
 */
model.DataService.getServiceUrl = function(serviceId) {
	var serviceUrl = "/DEV204";
	if (serviceId == undefined) {
		serviceId = "/oDataAPI";
	}
	serviceUrl = serviceUrl + serviceId + "/";
	return serviceUrl;
};
/**
 * @param serviceId
 * @returns {sap.ui.model.odata.ODataModel}
 * @lends model.DataService.prototype
 */
model.DataService.getModel = function(serviceId) {
	var model;
	var modelId;

	if (serviceId) {
		modelId = serviceId;
	} else {
		modelId = "mainModel";
	}

	serviceUrl = this.getServiceUrl(serviceId);
	model = sap.ui.getCore().getModel(modelId);
	if (model !== undefined) {
		return model;
	} else {
		model = new sap.ui.model.odata.ODataModel(this.getServiceUrl(serviceId), true);
		sap.ui.getCore().setModel(model, modelId);
		return model;
	}
};
/**
 * 
 * @param data
 * @param response
 * @param dataObject
 * @lends model.DataService.prototype
 */
model.DataService.loadSuccess = function(data, response, dataObject) {
	var resultData;
	var headers;

	headers = response.headers;
	serviceUrl = this.getServiceUrl();

	if (data) {

		if (data.results !== undefined) {
			// It is a collection
			resultData = data.results;
		} else {
			// It is not a collection
			resultData = data;
		}

	} else {
		// its a delete so no response body...
		resultData = {};
	}
	sap.ui.getCore().getEventBus().publish("data", dataObject.serviceType, {
		results : resultData,
		dataObject : dataObject,
		headers : headers,
		response : response
	});
};
/**
 * 
 * @param data
 * @param url
 * @param dataObject
 * @lends model.DataService.prototype
 */
model.DataService.loadError = function(data, url, dataObject) {

	if (data.response.statusCode == "500" || data.response.statusCode == "400") {
		// Internal Server Error
		if (dataObject.errServiceType) {
			sap.ui.getCore().getEventBus().publish("err", dataObject.errServiceType, data);
		} else {
			sap.ui.getCore().getEventBus().publish("err", dataObject.serviceType, data);
		}
	} else if (data.response.statusCode == "408") {
		// Time out
		var model = this.getModel();
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oStorage.remove("sessionId");
		model.setHeaders("");
		sap.ui.getCore().getEventBus().publish("err", dataObject.serviceType, data);
	} else {
		sap.ui.getCore().getEventBus().publish("err", "communicationFailure");
		throw new Error("Error during loading data." + "\n \n" + "Service type = " + dataObject.serviceType + "\n \n" + "Url = " + url + "\n \n" + "data object = " + JSON.stringify(dataObject));
	};
};
/**
 * 
 * @param data
 * @param response
 * @param dataObject
 * @lends model.DataService.prototype
 */
model.DataService.returnResponse = function(data, response, dataObject) {
	if (response.data !== undefined) {
		sap.ui.getCore().getEventBus().publish("data", dataObject.serviceType, {
			dataObject : response.data,
			headers : response.headers,
			type : dataObject.type,
			extraData : dataObject.extraData,
		});
	} else {
		sap.ui.getCore().getEventBus().publish("data", dataObject.serviceType, {
			dataObject : dataObject.odata,
			headers : response.headers,
			type : dataObject.type,
			extraData : dataObject.extraData,
		});
	}
};

model.DataService.createData = function(content, url, context, parameterArray, serviceId) {
	var model;
	var oData = {};
	var thisInstance = this;

	model = this.getModel();
	oData = content.oData;
	if (!content.headers == undefined) {
		if (content.headers.pyd_frw_sessionid) {
			model.setHeaders(content.headers);
		}
	};
	model.create(url, oData, context, function(data, response) {
		thisInstance.returnResponse(data, response, {
			serviceType : content.serviceType,
			type : content.serviceType,
		});
	}, function(oError) {
		thisInstance.loadError(oError, url, {
			serviceType : content.serviceType
		});
	});
},
/**
 * 
 * @param content
 * @param url
 * @param context
 * @param parameterArray
 * @param serviceId
 * @lends model.DataService.prototype
 */
model.DataService.loadData = function(content, url, context, parameterArray, serviceId) {
	var model;
	var thisInstance;

	thisInstance = this;
	model = this.getModel(serviceId);
	if (content.headers == undefined) {

	} else {
		if (content.headers.pyd_frw_sessionid) {
			model.setHeaders(content.headers);
		}
	};

	model.read(url, context, parameterArray, true, function(data, response) {
		thisInstance.loadSuccess(data, response, {
			serviceType : content.serviceType,
			errServiceType : content.errServiceType,
			params : content
		});
	}, function(oError) {
		thisInstance.loadError(oError, url, {
			serviceType : content.serviceType,
			errServiceType : content.errServiceType,
			params : content
		});
	});
};

model.DataService.updateData = function(content, url, context, parameterArray, serviceId) {
	var model;
	var oData = {};
	var thisInstance = this;

	model = this.getModel();
	oData = content.oData;
	if (!content.headers == undefined) {
		if (content.headers.pyd_frw_sessionid) {
			model.setHeaders(content.headers);
		}
	};
	model.update(url, oData, context, function(data, response) {
		thisInstance.returnResponse(data, response, {
			serviceType : content.serviceType,
			type : content.serviceType,
			params : content
		});
	}, function(oError) {
		thisInstance.loadError(oError, url, {
			serviceType : content.serviceType,
			params : content
		});
	}, true);
},

model.DataService.deleteData = function(content, url, context, parameterArray, serviceId) {
	var model;
	var thisInstance;

	thisInstance = this;
	model = this.getModel(serviceId);
	if (content.headers == undefined) {

	} else {
		if (content.headers.pyd_frw_sessionid) {
			model.setHeaders(content.headers);
		}
	};

	model.remove(url, context, function(data, response) {
		thisInstance.loadSuccess(data, response, {
			serviceType : content.serviceType,
			errServiceType : content.errServiceType,
			params : content
		});
	}, function(oError) {
		thisInstance.loadError(oError, url, {
			serviceType : content.serviceType,
			errServiceType : content.errServiceType,
			params : content
		});
	}, '', '', false, parameterArray);
};

model.DataService.getMetadata = function(serviceId) {
	var model = this.getModel(serviceId);
	if (model.oMetadata.oMetadata == null) {
		return null;
	} else {
		return model.oMetadata.oMetadata.dataServices.schema[0];
	}
};
