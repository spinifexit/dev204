package com.dbbrowser.service;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import org.apache.olingo.odata2.jpa.processor.api.ODataJPAContext;
import org.apache.olingo.odata2.jpa.processor.api.ODataJPAServiceFactory;
import org.apache.olingo.odata2.jpa.processor.api.exception.ODataJPARuntimeException;
import org.eclipse.persistence.config.PersistenceUnitProperties;

public class DatabaseService extends ODataJPAServiceFactory {

	private EntityManagerFactory emf;
	private String persistenceUnitName = "DEV204";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DatabaseService() {
		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

			Map properties = new HashMap();
			properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
			properties.put("eclipselink.ddl-generation", "create-or-extend-tables");
			emf = Persistence.createEntityManagerFactory(persistenceUnitName, properties);
		} catch (NamingException e) {

		}
	}

	@Override
	public ODataJPAContext initializeODataJPAContext()
			throws ODataJPARuntimeException {
		ODataJPAContext oDataJPAContext = this.getODataJPAContext();

		try {
			oDataJPAContext.setEntityManagerFactory(emf);
			oDataJPAContext.setPersistenceUnitName(persistenceUnitName);
			return oDataJPAContext;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
