package com.dbbrowser.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Session implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5214278760672642072L;

	/** The id. */
	@Id
	@GeneratedValue
	private Long id;
	
	/** The first name. */
	@Column(name = "sessionName")
	private String				sessionName;

	/** The last name. */
	@Column(name = "sessionCode")
	private String				sessionCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public String getSessionCode() {
		return sessionCode;
	}

	public void setSessionCode(String sessionCode) {
		this.sessionCode = sessionCode;
	}	
	
	
}
