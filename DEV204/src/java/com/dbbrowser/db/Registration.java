package com.dbbrowser.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Registration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4065781691945916995L;

	/** The id. */
	@Id
	@GeneratedValue
	private Long id;
	
	@JoinColumn(name = "participant")
	@ManyToOne
	private Participant participant;
	
	@JoinColumn(name = "session")
	@ManyToOne
	private Session session;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	
}
