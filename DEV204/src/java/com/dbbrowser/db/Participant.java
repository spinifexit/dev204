package com.dbbrowser.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Participant implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5889713594678893831L;

	/** The id. */
	@Id
	@GeneratedValue
	private Long id;
	
	/** The first name. */
	@Column(name = "firstName")
	private String				firstName;

	/** The last name. */
	@Column(name = "lastName")
	private String				lastName;	

	/** The email. */
	@Column(name = "email")
	private String				email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
